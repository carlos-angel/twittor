import axios from "axios";
import endpoints from "api/endpoints.api";

export async function uploadBannerApi(file) {
  try {
    const formData = new FormData();
    formData.append("banner", file);
    const { data } = await axios.post(endpoints.profile.uploadBanner, formData);
    return { error: false, data, message: "" };
  } catch (error) {
    return {
      error: true,
      data: error.response.data,
      message: error.response.data.message,
    };
  }
}

export async function uploadAvatarApi(file) {
  try {
    const formData = new FormData();
    formData.append("avatar", file);

    const { data } = await axios.post(endpoints.profile.uploadAvatar, formData);
    return { error: false, data, message: "" };
  } catch (error) {
    return {
      error: true,
      data: error.response.data,
      message: error.response.data.message,
    };
  }
}

export async function updateProfileApi(body) {
  try {
    const { data } = await axios.put(endpoints.profile.update, body);
    return { error: false, data, message: "datos actualizados" };
  } catch (error) {
    return {
      error: true,
      data: error.response.data,
      message: error.response.data.message,
    };
  }
}

export async function getFollowings({ id, page }) {
  try {
    const { data } = await axios.get(
      endpoints.profile.followings({ id, page }),
    );
    return { error: false, data, message: "" };
  } catch (error) {
    return {
      error: true,
      data: error.response.data,
      message: error.response.data.message,
    };
  }
}

export async function getFollowers({ id, page }) {
  try {
    const { data } = await axios.get(endpoints.profile.followers({ id, page }));
    return { error: false, data, message: "" };
  } catch (error) {
    return {
      error: true,
      data: error.response.data,
      message: error.response.data.message,
    };
  }
}

export async function getProfile({ id }) {
  try {
    const { data } = await axios.get(endpoints.profile.get({ id }));
    return { error: false, data, message: "" };
  } catch (error) {
    return {
      error: true,
      data: error.response.data,
      message: error.response.data.message,
    };
  }
}

export async function searchProfile({ query }) {
  try {
    const { data } = await axios.get(endpoints.profile.search({ query }));
    return { error: false, data, message: "" };
  } catch (error) {
    return {
      error: true,
      data: error.response.data,
      message: error.response.data.message,
    };
  }
}
