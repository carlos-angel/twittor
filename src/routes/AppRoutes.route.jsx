import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { Auth } from "pages/Auth";
import PrivateRoutes from "routes/PrivateRoutes.route";
import PublicRoutes from "routes/PublicRoutes.route";
import DashboardRoutes from "routes/DashboardRoutes.route";
import { useAuth } from "hooks/useAuth.hook";

export default function AppRoutes() {
  const { isAuthenticated } = useAuth();
  return (
    <BrowserRouter>
      <Routes>
        <Route
          path="/login"
          element={
            <PublicRoutes isAuthenticated={isAuthenticated}>
              <Auth />
            </PublicRoutes>
          }
        />
        <Route
          path="/*"
          element={
            <PrivateRoutes isAuthenticated={isAuthenticated}>
              <DashboardRoutes />
            </PrivateRoutes>
          }
        />
      </Routes>
    </BrowserRouter>
  );
}
