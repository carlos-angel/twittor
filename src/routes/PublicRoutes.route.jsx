import React from 'react';
import { Navigate } from 'react-router-dom';

export default function PublicRoutes({ children, isAuthenticated }) {
  return isAuthenticated ? <Navigate to='/' /> : children;
}
