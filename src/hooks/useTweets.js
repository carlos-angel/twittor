import { useEffect, useState } from "react";
import { isEmpty } from "lodash";
import { getTweetsApi } from "api/tweet.api";

export const useTweets = ({ endpoint }) => {
  const [tweets, setTweets] = useState([]);
  const [page, setPage] = useState(1);
  const [loading, setLoading] = useState(false);
  const [noMoreTweets, setNoMoreTweets] = useState(false);

  useEffect(() => {
    setLoading(true);

    getTweetsApi({ endpoint: `${endpoint}?page=${page}` })
      .then(({ error, data }) => {
        if (!error) {
          if (!isEmpty(data)) setTweets([...tweets, ...data]);
          else setNoMoreTweets(true);
        }
        setLoading(false);
      })
      .catch(() => setTweets([]));
  }, [endpoint, page]);

  const changePage = () => {
    setPage(page + 1);
  };

  return [tweets, changePage, loading, noMoreTweets];
};
