import { useState, useEffect } from 'react';
import { isEmpty } from 'lodash';
import { fetchWithToken } from 'helpers/fetch';

export const useUsers = (pathUrl) => {
  const [url, setUrl] = useState(pathUrl);
  const [page, setPage] = useState(1);
  const [loading, setLoading] = useState(false);
  const [users, setUsers] = useState([]);
  const [noMoreUsers, setNoMoreUsers] = useState(false);

  useEffect(() => {
    setLoading(true);
    fetchWithToken(`${url}?page=${page}`)
      .then((resp) => resp.json())
      .then((data) => {
        isEmpty(data) ? setNoMoreUsers(true) : setUsers((items) => [...items, ...data]);

        setLoading(false);
      })
      .catch(() => {
        setUsers([]);
        setLoading(false);
      });
  }, [url, page]);

  const changeUrlTypeUsers = (path) => {
    setUsers([]);
    setUrl(path);
    setPage(1);
    setNoMoreUsers(false);
  };

  const nextPage = () => {
    setPage(page + 1);
  };

  return [users, loading, noMoreUsers, nextPage, changeUrlTypeUsers];
};
