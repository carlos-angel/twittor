const baseUrl = import.meta.env.VITE_BASE_URL;

export const fetchWithToken = (endpoint, data = {}, method = "GET") => {
  const url = `${baseUrl}/${endpoint}`;
  const headers = {
    "Content-Type": "application/json",
    Authorization: `Bearer ${localStorage.getItem("token") || ""}`,
  };

  if (method === "GET") {
    return fetch(url, {
      method,
      headers,
    });
  }
  return fetch(url, {
    method,
    headers,
    body: JSON.stringify(data),
  });
};

export const fetchWithTokenAndSendFile = (
  endpoint,
  data = {},
  method = "GET",
) => {
  const url = `${baseUrl}/${endpoint}`;
  const headers = {
    Authorization: `Bearer ${localStorage.getItem("token") || ""}`,
  };

  if (method === "GET") {
    return fetch(url, {
      method,
      headers,
    });
  }
  const formData = new FormData();
  formData.append(data.fileName, data.file);

  return fetch(url, {
    method,
    headers,
    body: formData,
  });
};

export const fetchWithoutToken = (endpoint, data, method = "GET") => {
  const url = `${baseUrl}/${endpoint}`;
  if (method === "GET") {
    return fetch(url);
  }
  return fetch(url, {
    method,
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  });
};
