import React from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { useDebouncedCallback } from "use-debounce";
import queryString from "query-string";
import { Spinner } from "react-bootstrap";
import { useExplore } from "hooks/useExplore";
import ListUsers from "components/ListUsers";

import "./styles.scss";

export default function Explore() {
  const navigate = useNavigate();
  const location = useLocation();
  const { query = "" } = queryString.parse(location.search);
  const [items, loading] = useExplore({ query });

  const onSearch = useDebouncedCallback((search) => {
    navigate(`/explore?query=${search}`);
  }, 400);

  return (
    <div className="users">
      <div className="users__title">
        <input
          type="text"
          placeholder="Busca en twittor"
          onChange={(e) => onSearch(e.target.value)}
        />
      </div>

      {loading ? (
        <div className="users__loading">
          <Spinner animation="border" variant="info" />
          Buscando
        </div>
      ) : (
        <ListUsers users={items} />
      )}
    </div>
  );
}
