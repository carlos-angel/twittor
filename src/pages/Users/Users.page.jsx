import React, { useState } from "react";
import { Spinner, ButtonGroup, Button } from "react-bootstrap";
import { useAuth } from "hooks/useAuth.hook";
import ListUsers from "components/ListUsers";
import "./styles.scss";
import { useFollowings } from "hooks/useFollowings.hook";
import { useFollowers } from "hooks/useFollowers.hook";

export default function Users() {
  const {
    user: { sub },
  } = useAuth();
  const [typeButton, setTypeButton] = useState("followings");
  const [followings, loadingFollowings, noMoreFollowings, nextPageFollowings] =
    useFollowings({ id: sub });
  const [followers, loadingFollowers, noMoreFollowers, nextPageFollowers] =
    useFollowers({ id: sub });

  const loading = loadingFollowings || loadingFollowers;
  const isFollowings = typeButton === "followings";

  const onChangeTypeButton = (type) => setTypeButton(type);
  const nextPage = () =>
    isFollowings ? nextPageFollowings() : nextPageFollowers();
  const noMore = () => (isFollowings ? noMoreFollowings : noMoreFollowers);

  return (
    <div className="users">
      <div className="users__title">
        <h2>Usuarios</h2>
      </div>

      <ButtonGroup className="users__options">
        <Button
          onClick={() => onChangeTypeButton("followings")}
          className={typeButton === "followings" && "active"}
        >
          Siguiendo
        </Button>
        <Button
          onClick={() => onChangeTypeButton("followers")}
          className={typeButton === "followers" && "active"}
        >
          Seguidores
        </Button>
      </ButtonGroup>

      {loading ? (
        <div className="users__loading">
          <Spinner animation="border" variant="info" />
          Buscando usuarios
        </div>
      ) : (
        <>
          <ListUsers users={isFollowings ? followings : followers} />
          {!noMore() && (
            <Button onClick={nextPage} className="load-more">
              {loading ? (
                <Spinner
                  as="span"
                  animation="grow"
                  size="sm"
                  role="status"
                  arian-hidden="true"
                />
              ) : (
                "obtener más usuarios"
              )}
            </Button>
          )}
        </>
      )}
    </div>
  );
}
