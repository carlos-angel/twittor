import { Formik, Form } from "formik";
import { Row, Col, FormGroup } from "react-bootstrap";
import { toast } from "react-toastify";
import * as Yup from "yup";
import { InputText } from "components/Form";
import { signUp } from "api/auth.api";
import Button from "components/UI/Button";
import "./styles.scss";

function initialValues() {
  return {
    name: "",
    email: "",
    password: "",
    confirmPassword: "",
  };
}

function validationSchema() {
  return Yup.object({
    name: Yup.string().required("name is required"),
    email: Yup.string()
      .email("Invalid email address")
      .required("email is required"),
    password: Yup.string()
      .min(8, "the password must be min 8 characters")
      .required("password is required"),
    confirmPassword: Yup.string()
      .min(8, "the password must be min 8 characters")
      .required("password is required")
      .oneOf([Yup.ref("password")], "the passwords must be equals"),
  });
}

export default function SignUpForm({ setShowModal }) {
  return (
    <div className="sign-up-form">
      <h2>Crea tu cuenta</h2>
      <Formik
        initialValues={initialValues()}
        validationSchema={validationSchema()}
        onSubmit={async ({ name, email, password }) => {
          const { error, message } = await signUp({
            name,
            email,
            password,
          });
          if (error) {
            toast.warning(message);
          } else {
            toast.success(message);
            setShowModal(false);
          }
        }}
      >
        {({ isSubmitting }) => (
          <Form>
            <FormGroup>
              <InputText type="text" name="name" placeholder="nombre" />
            </FormGroup>

            <FormGroup>
              <InputText
                type="email"
                name="email"
                placeholder="correo electrónico"
              />
            </FormGroup>

            <FormGroup>
              <Row>
                <Col>
                  <InputText
                    type="password"
                    name="password"
                    placeholder="contraseña"
                  />
                </Col>
                <Col>
                  <InputText
                    type="password"
                    name="confirmPassword"
                    placeholder="confirmar contraseña"
                  />
                </Col>
              </Row>
            </FormGroup>

            <Button variant="primary" type="submit" loading={isSubmitting}>
              Registrarse
            </Button>
          </Form>
        )}
      </Formik>
    </div>
  );
}
