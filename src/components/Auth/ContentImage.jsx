import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faSearch,
  faUsers,
  faComment,
} from '@fortawesome/free-solid-svg-icons';
import Logo from 'assets/png/logo.png';

function ContentImage() {
  return (
    <>
      <img src={Logo} alt="twittor" />
      <div>
        <h2>
          <FontAwesomeIcon icon={faSearch} />
          Sigue lo que te interesa
        </h2>
        <h2>
          <FontAwesomeIcon icon={faUsers} />
          {' '}
          Entérate de que está pasando en el
          mundo
        </h2>
        <h2>
          <FontAwesomeIcon icon={faComment} />
          {' '}
          Únete a la conversación
        </h2>
      </div>
    </>
  );
}

export default ContentImage;
