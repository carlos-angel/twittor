import React from "react";
import { MoreInfo } from "./MoreInfo";
import "./styles.scss";

export function Information({ user }) {
  return (
    <div className="information-user">
      <h2 className="name">{user.name}</h2>
      <p className="email">{user?.email}</p>
      {user?.biography && <div className="description">{user.biography}</div>}

      <MoreInfo>
        {user?.location && <MoreInfo.Location location={user.location} />}
        {user?.website && <MoreInfo.WebSite website={user.website} />}
        {user?.birthday && <MoreInfo.Birthday birthday={user.birthday} />}
      </MoreInfo>
    </div>
  );
}
