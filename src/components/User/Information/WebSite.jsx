import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faLink } from '@fortawesome/free-solid-svg-icons';

export function WebSite({ website }) {
  return (
    <a href={website} alt={website} target="_blank" rel="noopener noreferrer">
      <FontAwesomeIcon icon={faLink} />
      {website}
    </a>
  );
}
