/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import DatePicker from 'react-datepicker';
import es from 'date-fns/locale/es';
import { useField, ErrorMessage } from 'formik';

export function InputDate(props) {
  const [{ value, ...field }] = useField(props);
  const { name } = props;
  return (
    <>
      <DatePicker {...field} {...props} locale={es} />
      <ErrorMessage
        name={name}
        render={(message) => <div className="invalid-feedback">{message}</div>}
      />
    </>
  );
}
