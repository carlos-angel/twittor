import React from 'react';
import { map, isEmpty } from 'lodash';
import ItemUser from 'components/ListUsers/ItemUser';
import './styles.scss';

export default function ListUsers({ users }) {
  if (isEmpty(users)) {
    return <div className="text-center pt-5">Sin resultados</div>;
  }

  return (
    <ul className="list-users">
      {map(users, (user) => (
        // eslint-disable-next-line react/jsx-props-no-spreading
        <ItemUser key={user.id} {...user} />
      ))}
    </ul>
  );
}
