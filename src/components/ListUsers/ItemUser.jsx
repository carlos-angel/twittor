import React from 'react';
import { Media, Image } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import AvatarNoFound from 'assets/png/avatar-no-found.png';

export default function ItemUser({
  id, name, avatar, biography = '',
}) {
  return (
    <Media as={Link} to={`/${id}`} className="list-users__user">
      <Image
        width={64}
        height={64}
        roundedCircle
        className="mr-3"
        src={avatar || AvatarNoFound}
        alt={`${name}`}
      />

      <Media.Body>
        <h5>{name}</h5>
        <p>{biography}</p>
      </Media.Body>
    </Media>
  );
}
