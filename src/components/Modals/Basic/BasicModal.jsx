import React from "react";
import ReactDOM from "react-dom";
import { Modal } from "react-bootstrap";
import LogoWhiteTwittor from "assets/png/logo-white.png";
import "./styles.scss";

export default function BasicModal({ show, setShow, children }) {
  return ReactDOM.createPortal(
    <Modal
      className="basic-modal"
      show={show}
      onHide={() => setShow(false)}
      centered
      size="lg"
    >
      <Modal.Header>
        <Modal.Title>
          <img src={LogoWhiteTwittor} alt="twittor" />
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>{children}</Modal.Body>
    </Modal>,
    document.getElementById("modal"),
  );
}
