import React from "react";
import { Modal } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes } from "@fortawesome/free-solid-svg-icons";
import Button from "components/UI/Button";
import classNames from "classnames";
import { tweetApi } from "api/tweet.api";
import { Formik, Form } from "formik";
import * as Yup from "yup";

import "./styles.scss";
import { toast } from "react-toastify";
import { InputText } from "components/Form";

function initialValues() {
  return {
    message: "",
  };
}

function validationSchema(maxLength) {
  return Yup.object({
    message: Yup.string().max(
      maxLength,
      `the message must be min ${maxLength} characters`,
    ),
  });
}

export default function TweetModal({ show, setShow }) {
  const maxLength = 280;

  return (
    <Modal
      className="tweet-modal"
      show={show}
      onHide={() => setShow(false)}
      centered
      size="lg"
    >
      <Modal.Header>
        <Modal.Title>
          <FontAwesomeIcon icon={faTimes} onClick={() => setShow(false)} />
          Tweet
        </Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <Formik
          initialValues={initialValues()}
          validationSchema={validationSchema(maxLength)}
          onSubmit={async (values) => {
            const { message } = values;
            try {
              const data = await tweetApi(message);
              if (data.error) toast.warning(data.message);
              else {
                toast.success(data.message);
                setShow(false);
              }
            } catch (err) {
              toast.warning(err.message);
            }
          }}
        >
          {({ values, isSubmitting }) => (
            <Form>
              <InputText
                as="textarea"
                name="message"
                placeholder="¿Qué está pensando?"
                rows="6"
              />

              <span
                className={classNames("count", {
                  error: values.message.length > maxLength,
                })}
              >
                {values.message.length}
              </span>

              <Button
                variant="primary"
                type="submit"
                loading={isSubmitting}
                disabled={
                  values.message.length > maxLength || values.message.length < 1
                }
              >
                Tweetoar
              </Button>
            </Form>
          )}
        </Formik>
      </Modal.Body>
    </Modal>
  );
}
