import React from 'react';
import { Image } from 'react-bootstrap';
import { map } from 'lodash';
import moment from 'moment';
import AvatarNotFound from 'assets/png/avatar-no-found.png';
import replaceURLWithHTMLLinks from 'utils/detected-url';
import './styles.scss';

export default function Tweets({ tweets }) {
  return (
    <div className="tweets">
      {map(tweets, (tweet, index) => (
        // eslint-disable-next-line react/jsx-props-no-spreading
        <Tweet key={index} {...tweet} />
      ))}
    </div>
  );
}

function Tweet({ message, published, user }) {
  return (
    <div className="tweet">
      <Image
        src={user?.avatar || AvatarNotFound}
        className="avatar"
        roundedCircle
      />
      <div>
        <div className="name">
          {`${user.name}`}
          <span>{moment(published).calendar()}</span>
        </div>
        <div
          // eslint-disable-next-line react/no-danger
          dangerouslySetInnerHTML={{ __html: replaceURLWithHTMLLinks(message) }}
        />
      </div>
    </div>
  );
}
