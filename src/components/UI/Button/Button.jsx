/* eslint-disable react/jsx-props-no-spreading */
import { Button as ButtonBootstrap, Spinner } from 'react-bootstrap';
import './Button.scss';

export default function Button(props) {
  const { loading, children } = props;
  return (
    <ButtonBootstrap {...props} loading={`${loading}`}>
      {loading ? <Spinner animation="border" /> : children}
    </ButtonBootstrap>
  );
}
