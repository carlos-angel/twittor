import { AuthProvider } from "context/auth";
import AppRoutes from "routes/AppRoutes.route";
import ToastConfig from "components/common/ToastConfig";

function App() {
  return (
    <AuthProvider>
      <ToastConfig />
      <AppRoutes />
    </AuthProvider>
  );
}
export default App;
